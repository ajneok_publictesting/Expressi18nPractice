var express = require("express");
var cookieParser = require("cookie-parser");
var i18n = require("./libraries/i18n");

var app = express();

app.set("views", __dirname + "/views");

// cookieParser
app.use(cookieParser());

// i18n middleware
app.use(i18n);

app.get("/", function (req, res) {
  res.render("index.ejs");
});
app.get("/about", function (req, res) {
  res.render("about.ejs");
});

app.listen("3000");
