const { I18n } = require("i18n");

const i18n = new I18n();

const locales = ["en", "tw"];

i18n.configure({
  locales: locales,
  directory: __dirname + "/../locales",
  defaultLocale: "tw",
  cookie: "nwLang",
  objectNotation: true,
  queryParameter: "swlang", // 內建queryParameter 不須自己寫middleware但是網址會有參數很醜...
});

module.exports = function (req, res, next) {
  i18n.init(req, res);

  if (typeof req.query.lang !== "undefined" &&
    locales.indexOf(req.query.lang.toLowerCase()) > -1
  ) {
    res.cookie("nwLang", req.query.lang);
    res.redirect(req.get("referer"));
  } else {
    return next();
  }
};
